﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Boliche;

namespace Teste
{
    [TestClass]
    public class UnitTest1
    {
        private JogoDeBoliche boliche;

        

        //verifica entradas zeradas
        [TestMethod]
        public void TestMethod1()
        {
            for (int i =0; i< 20; i++)
            {
                boliche.Jogar(0);
                Assert.AreEqual(0, boliche.ObterPontuacao());
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            boliche = new JogoDeBoliche();
        }
    }
}
