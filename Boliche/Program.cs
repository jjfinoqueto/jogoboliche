﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boliche
{
    class Program
    {
        static void Main(string[] args)
        {
            JogoDeBoliche jogo = new JogoDeBoliche();
            int turno = 1;
            Console.Write("Olá, este é um jogo de simulação de boliche. \nLembre-se que os pinos derrubados devem ser de 0 a 10 em cada jogada, e que a cada turno no máximo podem ser derrubados 10 pinos. \nDigite os pinos derrubados em cada jogada e pressione enter.\n\n");
            
            while (turno<11)
            {
                Console.Write("\nTurno: " + turno);
                Console.Write("\nJogada 1. Pinos derrubados: " );
                int jogada1;
                while (!Int32.TryParse(Console.ReadLine().Replace(" ", ""), out jogada1))
                {
                    Console.Write("\n   Erro! Digite um valor válido de 0 a dez: ");
                }
                if (jogada1 > 10 || jogada1<0)
                {
                    Console.Write("\n   Erro! Sua jogada foi inválida, acabou não derrubando nenhum pino!");
                    jogada1 = 0;
                }
                int jogada2 = 0;
                //se fez Strike não entra para segunda jogada do turno
                if (!(jogada1 == 10))
                {
                    Console.Write("\nJogada 2. Pinos derrubados: ");
                    while (!Int32.TryParse(Console.ReadLine().Replace(" ", ""), out jogada2))
                    {
                        Console.Write("\n   Erro! Digite um valor válido de 0 a 10: ");
                    }
                    if (jogada2 + jogada1>10)
                    {
                        Console.Write("\n   Erro! Sua segunda jogada foi inválida, acabou não derrubando nenhum pino!");
                        jogada2 = 0;
                    }
                }
                jogo.Jogar(jogada1);
                if (jogada2 > 0)
                {
                    jogo.Jogar(jogada2);
                }

                if (turno == 10 && (jogada1)== 10)
                {
                    //strikeSpareNoUltimo = true;
                    Console.Write("Strike na última rodada! Você ganhou 2 jogadas Extras! \nPinos derrubados na jogada extra 1: ");
                    jogada1 = int.Parse(Console.ReadLine());
                    jogo.Jogar(jogada1);
                    Console.Write("\nPinos derrubados na jogada extra 2: ");
                    jogada1 = int.Parse(Console.ReadLine());
                    jogo.Jogar(jogada1);
                 
                }
                turno++;
                Console.Write("\n---------------------------------------------------------------");
            }
           
            
            Console.Write("Pontuação total:" + jogo.ObterPontuacao());
            Console.Read();

        }
        

    }
    public class JogoDeBoliche
    {
        private int[] Jogadas = new int[22];
        private int JogadaAtual;

        public void Jogar(int pinosDerrubados)
        {
            Jogadas[JogadaAtual++] = pinosDerrubados;
            
        }

        public int ObterPontuacao()
        {
            int pontuacao = 0;
            int indice = 0;
            for (int i = 0; i < 10; i++)
            {
                if (ChecaStrike(indice))
                {
                    pontuacao += SomaParaStrike(indice);
                    indice++;
                }
                else if (ChecaSpare(indice))
                {
                    pontuacao += SomaParaSpare(indice);
                    indice += 2;
                }
                else
                {
                    pontuacao += SomaRodada(indice);
                    indice += 2;
                }
            }


            return pontuacao;
        }

        private int SomaParaStrike(int indice)
        {
            return Jogadas[indice] + Jogadas[indice + 1] + Jogadas[indice + 2];
        }

        private bool ChecaSpare(int indice)
        {
            return Jogadas[indice] + Jogadas[indice + 1] == 10;
        }

        private bool ChecaStrike(int indice)
        {
            return Jogadas[indice] == 10;
        }

        private int SomaRodada(int indice)
        {
            return Jogadas[indice] + Jogadas[indice + 1];
        }

        private int SomaParaSpare(int indice)
        {
            
            return Jogadas[indice] + Jogadas[indice + 1] + Jogadas[indice + 2];
        }


    }
}
